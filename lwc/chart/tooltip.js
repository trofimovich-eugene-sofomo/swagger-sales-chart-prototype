const getOrCreateTooltip = (chart) => {
  let tooltipEl = chart.canvas.parentNode.querySelector('div');

  if (!tooltipEl) {
    tooltipEl = document.createElement('div');
    tooltipEl.classList.add('chartjs-tooltip');
    tooltipEl.style.background = 'rgba(0, 0, 0, 0.7)';
    tooltipEl.style.borderRadius = '3px';
    tooltipEl.style.color = 'white';
    tooltipEl.style.opacity = 1;
    // tooltipEl.style.pointerEvents = 'none';
    tooltipEl.style.position = 'absolute';
    tooltipEl.style.padding = '6px';
    tooltipEl.style.transform = 'translate(-50%, 0)';
    tooltipEl.style.transition = 'all .1s ease';

    const table = document.createElement('table');
    table.style.margin = '0px';

    tooltipEl.appendChild(table);
    chart.canvas.parentNode.appendChild(tooltipEl);
  }

  return tooltipEl;
};

const extractExternalTooltipData = (tooltip) => {
  const { dataIndex } = tooltip.dataPoints[0];
  return tooltip.dataPoints[0].dataset.externalTooltipData[dataIndex];
}
  
const externalTooltipHandler = (context) => {
  // Tooltip Element
  const {chart, tooltip} = context;
  const externalTooltipData = extractExternalTooltipData(tooltip);
  const tooltipEl = getOrCreateTooltip(chart);

  // Hide if no tooltip
  if (tooltip.opacity === 0) {
    tooltipEl.style.opacity = 0;
    return;
  }

  if (tooltip.body) {
    const tableRoot = tooltipEl.querySelector('table');
    tableRoot.innerHTML = `
      <tr>
        <td colspan="2" style="padding-bottom: 6px">
          <strong>Meeting number ${externalTooltipData.meetingNumber}</strong>
        </td>
      </tr>
      <tr>
        <td><strong>Call Plan</strong></td>
        <td><strong>Meeting Date</strong></td>
      </tr>
      <tr>
        <td style="padding-right: 20px">
          <a
            style="color:lightsteelblue"
            rel='nofollow'
            target='_blank'
            href='${externalTooltipData.meetingLink}'
          >
            Meeting ${externalTooltipData.meetingNumber}
          </a>
        </td>
        <td style="padding-bottom: 6px">
          ${externalTooltipData.date}
        </td>
      </tr>
      <tr>
        <td><strong>Acount</strong></td>
        <td><strong>Plan owner</strong></td>
      </tr>
      <tr>
        <td>
          ${externalTooltipData.account}
        </td>
        <td>
          ${externalTooltipData.planOwner}
        </td>
      </tr>`;
  }

  const {offsetLeft: positionX, offsetTop: positionY} = chart.canvas;

  // Display, position, and set styles for font
  tooltipEl.style.opacity = 1;

  if (positionX + tooltip.caretX < tooltipEl.offsetWidth / 2) {
    tooltipEl.style.left = positionX + tooltip.caretX + tooltip.width + 'px';
  } else {
    tooltipEl.style.left = positionX + tooltip.caretX + 'px';
  }
  tooltipEl.style.top = positionY + tooltip.caretY + 'px';
  tooltipEl.style.font = tooltip.options.bodyFont.string;
  tooltipEl.style.padding = tooltip.padding + 'px ' + tooltip.padding + 'px';
};

export default externalTooltipHandler;
