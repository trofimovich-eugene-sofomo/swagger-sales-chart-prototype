import { LightningElement, api, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart';
import moment from '@salesforce/resourceUrl/momentjs'
import chartjsadaptermoment from '@salesforce/resourceUrl/chartjsadaptermoment';
import ResizeObserver from '@salesforce/resourceUrl/ResizeObserver';
import externalTooltipHandler from './tooltip';

const getData = (chartdata) => {
    const colors = ['blue', 'gray', 'orange', 'green', 'red'];
    const labels = [
        new Date('2020-01-01'),
        new Date('2020-01-02'),
        new Date('2020-01-03'),
        new Date('2020-01-04'),
        new Date('2020-01-05'),
        new Date('2020-01-06'),
        new Date('2020-01-07'),
        new Date('2020-01-08'),
        new Date('2020-01-09'),
        new Date('2020-01-10'),
        new Date('2020-01-11'),
        new Date('2020-01-12'),
        new Date('2020-01-13'),
        new Date('2020-01-14'),
        new Date('2020-01-15'),
        new Date('2020-01-16'),
        new Date('2020-01-17'),
    ]; // TODO: extract labels from chart (MIN-MAX) OR from exertnal date range;
    const datasets = chartdata.map((seria, index) => ({
        label: seria.label,
        borderColor: colors[index],
        backgroundColor: colors[index],
        data: seria.data.map(seriaData => ({
            x: new Date(seriaData.date),
            y: seriaData.value,
        })),
        externalTooltipData: seria.data.map(({ date, meetingNumber, meetingLink, account, planOwner }) => ({
            date,
            meetingNumber,
            meetingLink,
            account,
            planOwner,
        })),
    }));

    return {
        labels,
        datasets,
    };
};

let chart;
let config = {
    type: 'line',
    data: [],
    options: {
        responsive: true,
        events: ['click'],
        animation: {
            animateScale: true,
            animateRotate: true
        },
        interaction: {
            intersect: false,
            mode: 'nearest',
        },
        scales: {
            x: {
                type: 'time',
                time: {
                  // Luxon format string
                  tooltipFormat: 'DD'
                },
            },
            y: {
                display: true,
            },
        },
        plugins: {
            legend: {
                title: {
                    display: true,
                    text: 'Competencies',
                },
                labels: {
                    boxWidth: 40,
                },
                position: 'right',
                maxWidth: 200,
            },
            tooltip: {
                enabled: false,
                position: 'nearest',
                external: externalTooltipHandler,
            }
        }
    }
};


export default class ChartPrototype extends LightningElement {
    chartjsInitialized = false;
    chart;

    @track _chartdata;
    @api chartdata;
    get chartdata() {
        return this._chartdata;
    }
    set chartdata(data) {
        if (this.chartjsInitialized) {
            chart.data = getData(JSON.parse(JSON.stringify(data)));
            chart.update();
        }

        this._chartdata = data;
    }

    connectedCallback() {
        document.addEventListener('click', () => {
            // hide tooltip and update chart on external click
            const tooltipEl = document.querySelector('.chartjs-tooltip');
            tooltipEl.remove();
            chart.tooltip._active = [];
            chart.tooltip._tooltipItems = [];
            chart.update();
        });
    }

    renderedCallback() {
        if (this.chartjsInitialized) {
            return;
        }

        Promise.all([
            loadScript(this, moment),
            loadScript(this, chartjs),
            loadScript(this, ResizeObserver),
        ]).then(() => {
            Promise.all([loadScript(this, chartjsadaptermoment)]).then(() => {
                config.data = getData(JSON.parse(JSON.stringify(this.chartdata))); // this.chartdata is being cloned in order to fix issue with access restriction in salesforce
                const ctx = this.template.querySelector('canvas.chart').getContext('2d')
                chart = new window.Chart(ctx, config);
                this.chartjsInitialized = true;
            })
        })
    }

    disconnectedCallback() {
        document.removeEventListener('click');
    }
}