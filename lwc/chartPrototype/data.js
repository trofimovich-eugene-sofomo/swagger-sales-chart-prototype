export default [
    {
        label: 'Overall',
        data: [
            {
                date: '2020-01-01',
                value: 10,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-03',
                value: 40,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Small Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-06',
                value: 55,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Huge Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-11',
                value: 30,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            }
        ]
    },
    {
        label: 'Plan',
        data: [
            {
                date: '2020-01-02',
                value: 20,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-04',
                value: 70,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-05',
                value: 10,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-17',
                value: 40,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            }
        ]
    },
    {
        label: 'Discover',
        data: [
            {
                date: '2020-01-01',
                value: 60,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-02',
                value: 20,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-03',
                value: 45,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-08',
                value: 100,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            }
        ]
    },
    {
        label: 'Purpose',
        data: [
            {
                date: '2020-01-03',
                value: 0,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-05',
                value: 40,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-10',
                value: 70,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            },
            {
                date: '2020-01-13',
                value: 10,
                meetingNumber: 20344,
                meetingLink: 'https://google.com',
                account: 'Big Tech Inc',
                planOwner: 'Marco Giangiotti',
            }
        ]
    }
];
