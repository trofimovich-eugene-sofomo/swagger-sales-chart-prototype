import { LightningElement, track } from 'lwc';
import seriesData from './data';


export default class ChartPrototype extends LightningElement {
    @track chartdata = seriesData;
    period = '1m';

    handlePeriodChange = (e) => {
        this.period = e.target.value;
        this.chartdata = seriesData.map(seria => ({ data: seria.data, label: 'U' + seria.label }));
    }

    connectedCallback() {

    }

    renderedCallback() {

    }

    disconnectedCallback() {
    }
}